---
title: Jean-Marc Jancovici at Mines Paris Tech, 2019 - Part I
subtitle: Courses 1 & 2
date: 1921-10-13
tags: ["online course", "energy", "climate change"]
---

[**Date:** 2021-10-13]  

> In this series of notes, I will try to summarize some of the key takeaways from the
course of Jean-Marc Jancovici on energy and climate change. I had the occasion to
listen to a talk of Mr Jancovici at the Ecole Normale Supérieur many years ago,
he is an expert on energy and climate change, he is now also a columnist on the famous
French radio RTL, among many other responsibilities. For more information, please visit
his website: [jancovici.com](https://jancovici.com/en/).
>
> Many plots are used in the slides of the course. Obviously, a graph is often a
better summary than a thousand words. Nevertheless, to avoid any problem with copyrights
and to keep the level less technical, I will avoid using many plots shown in the course
and limit the technical material. In addition, these notes are not a full exhaustive
coverage of the course, but are more a subset of the course for which I have though
that taking notes could be interesting for the reader and myself.
For example, the course being held in France, it contains numerous references to the
French energetic mix, for which not every fact is reported.
>
> _**Disclaimer:**_ most of the post content reflect statements made in the course and
not my own belief (even when my own belief may be aligned with the content).
Since the results presented here are mainly taken from the course itself and not produced
by my own analysis, this text should not be seen as the result of my work,
but rather a report on the course made through my own filter, focusing on what I believe
to be more relevant for this blog.
>
> **You can find all the videos and slides of the courses, including the ones that will be
covered in future posts:**
> - [Videos of the courses](https://www.youtube.com/results?search_query=cours+Jancovici)
> - [Slides of the courses](https://cloud.mines-paristech.fr/index.php/s/nCzdipz0uhIlTlw)



# COURSE 1 - From Big Bang to nowadays

**Video link (in French):** [video course 1](https://www.youtube.com/watch?v=WQJsMmJMKOo)


Most exams during scientific trainings, e.g. at university, focus on _"well posed
problems with an exact solution"_, while most problems in life are _"not well posed
(missing information) and with multiple solutions"_. Hence, every theory and solution
should be confronted with simple order of magnitudes reasonings (very important in
physics) and simple cross-multiplications (when systems are in a linear regime).

Questions around energy often fill medias reports, but one can ask how important energy is
in our societies? When seen at a global scale, the ratio energy consumption / GDP is
pretty low: between 0.5% and 1% for coal, 0.5% to 2% for gas, and 1%
to 9% for oil, depending on the years under consideration.

In terms of economical considerations, energy thus takes a small amount of consumption,
hence occupying a small part of political concerns and debates compared to other topics.
However, representing energy from its slice of global GDP is a mistake, as all other
slices of GDP depend on energy. When looking at a pie chart, we must always wonder if
there isn't a hidden hierarchy between the slices. Hence, without energy there is no
production of wealth in the same way as one cannot live without agricultural activities,
despite its small portion of economic activities.

What is energy? Energy is a _physical quantity that allows the change in states of a
physical system_. In other worlds, energy is what quantifies the change of state of a
system. In terms of human civilizations, energy can then be seen as our capacity to
change the world around us.

Machines that we build all bring a transformation, modifying temperature, speed,
shape of things, chemical composition, positions in physical fields (e.g. magnetic,
gravitational), atomic composition, or even other energies.

Short history of energies discovery:
- fire around 500,000 B.C. 
- wood, wind, solar, hydraulic, animal strength in antiquity.
- oil by Sumerians around 3,000 B.C. 
- coal in China around 1,000 B.C.
- nuclear in the 1950s.

The First law of thermodynamics implies that we can only use energy by finding it in
the environment and transform it, we cannot create it. In that case, we call this energy
a "primary energy" (c.f. [Wiki's definition](https://en.wikipedia.org/wiki/Primary_energy))
and it is transformed into a "final energy" through an energy convertor.

Modern age is not really characterised by new sources of energies (except for nuclear
and photovoltaic energies), it is about massive usage of some of them. Also, renewable
energies are not the energy of the future, they rather are the only source of energy
conversion that humans had for thousands of years until the discovery of fossil and
nuclear energies (roughly until three centuries ago), rediscovered through the use of
new technologies. 

The economical term "consumption" is in fact almost synonymous with "usage of
machines" when we take a physical perspective. Indeed, every common activity of
our daily life involves hundreds of machines that has been used to allow this activity.
All the machines use energy, so energy is at the center of all human activities.


When discussing energies, several units are often used:
- 1 kWh (kilo Watt hour) ≃ 3.6 MJ (Mega Joule).
- 1 L of oil ≃ 10 kWh.
- Some specialists like to use the "ton of oil equivalent" (toe) which is the energy contained in a
ton of crude oil: 1 toe ≃ 41.868 GJ ≃ 11,600 kWh.
- 1 eV = 1.6 E-18 J.
- "slave energy" ([Wiki's definition](https://en.wikipedia.org/wiki/Energy_slave)): 1 ES ≃ 100 kWh.

As an example, a French citizen consumes on average 170 GJ of energy per year. This is
equivalent to roughly 50,000 kWh / year. French people use about 1000 L of oil per year. 

People consume energy for their daily life, but people also produce work from food they
eat. The energy produced by human legs is about 0.5 kWh and by arms is about 0.05 kWh
for a day of work (power of legs is about 100 W and power of arms about 10 W,
assuming 5 hours of work per day). So a simple calculation shows that a human being
can produce of the order of 100 kWh / year through legs activity and of the order of
10 kWh / year through arms activity. On the other hand, 1L of gasoline produces 10 kWh,
but machines have limited efficiency that reduces it to about 2-4 kWh.
Using 4 kWh for evaluation, we find that a machine using one liter of oil produces
8 times more energy than someone employing his/her legs for a day,
or about 80 people using their hands during a day. In summary, the cost associated with
the use of a machine compared to human labor is divided by several hundreds or thousands.

Comparing powers of different entities:
- human: legs 100 W, arms 10 W.
- tractor: 60 kW.
- caterpillar: 100 kW.
- truck: 400 kW.
- plane: 100 MW.

One can understand from these numbers why a farmer today can do the handwork of a hundred
people and why we can now build houses for the price of few years of salary.

In general, every type of energy has pros and cons. Hence, a clean energy is an energy
for which counterparts are acceptable to us (at the present moment and forseeable future).
Every source of primary energy is free, what has a cost is the right to exploit it and the
cost to extract it. When an energy is not "controllable" (meaning that we can use it at
will at the desired time) and diffuse, it will cost more to extract it. This explains
why oil is so cheap and why energy from renewables may be more expensive.

Oil is a very concentrated source of energy. In comparison, 1000 m^3 of air with a
speed of 80 km/h, completely captured by a wind turbine (hypothetically) is equivalent
to the energy produced by 3 mL of oil. (_Remark:_ at this speed, this volume passes
through the turbine in 0.45 sec. After, another volume can go through. Following the
reasoning, a full day of wind blowing at this speed would produce the equivalent of more
than 500 L of oil). This brings the price of a kWh from wind turbine extraction around
6-8 euro cents, about 3-4 times more to store it, while oil brings it to 0.3 euro cents. Hence, the factor is about 10 to 100. This is why boats don't use wind anymore or why agriculture uses tractors instead of animals.

Analyzing the global production of energy, we find the following evolution:

| ![Global energy consumption per capita](https://fabien-nugier.gitlab.io/blog/post/blog3/kWh_per_capita_global.jpg) |
|:--:|
| <b>Global energy consumption per capita. Source: jancovici.com.</b>|

When we look at the composition of kWh per capita above, we see that humans use less
wood over the years, but the use of coal did not decrease globally. In particular, 2/3
of coal is used for electricity production (and half of the rest for steel production),
and there are more than 6,500 coal power plants in the world (c.f. e.g. the [Bloomberg Global Coal Countdown](https://bloombergcoalcountdown.com/)
for more details), representing about 2,000 GW of production capacity.
Oil never replaced coal for producing energy as oil is essentially used for transport.
This is because oil has the best ratio of transported energy by units of volume, and it
is easy to transport. But the electric engine is about 4 times more efficient than oil
engine. So it is not the efficiency of oil engines which justified the use of oil in
cars, but the easiness of transporting energy through oil. Hence, we understand that the
main problem of electric cars is not the engine, but the batteries.

We can see that the energy used by a person globally today is about 20,000 kWh per year
(significantly less than a French citizen). This is about 200 ES, meaning that all the
sources of energies we use represent (very roughly) the work of 200 people working 24h
per day, 365 days a year.

Contrary to what we may think from media exposure, on the period 2000-2017, by far most
of new energy sources produced globally are extracted from fossil energies, especially
coal which rose 14 times faster than photovoltaic and 6 times faster than wind power:

| ![Global energy consumption change](https://fabien-nugier.gitlab.io/blog/post/blog3/energy_change.jpg) |
|:--:|
| <b>Global energy consumption change. Source: jancovici.com.</b>|

Among renewable energies, the most important energies are by far wood + biomass and
hydro-electricity, and far being wind, geothermal, photovoltaic and others. In terms of
numbers, we have in 2015 in shares of world energy total:
- wood and biomass: 8.4%
- hydroelectricity: 6.2%
- wind: 1.3%
- geothermal: 0.8%
- biofuels: 0.5%
- municipal waste: 0.4%
- photovoltaic: 0.4%

The price of oil (corrected from inflation) has fluctuated a lot over time and shows
that it is not really related to offer and demands. As for the price of other energies,
they all depend to some extent on the price of oil itself. During the last 100 years,
the price of 1 kWh was divided by a factor 30-50 in the western world, which is roughly
the ratio of energy provided by oil over renewables, as explained above.
Fluctuations in the price of oil, especially during oil crises, had a significant
impact in sectors of production and evolution of societies. Availability of oil
implies the use of more and more machines, which correlates with the decrease in
the portion of farmers in a population and the increase of urban populations.
This trend had a significant impact, crops now mostly growing food for animal
husbandry and price of food in people's budget being significantly reduced.

Use of machines through the low cost of energy, especially through oil, and their
economic impact also had significant consequences on the development of tertiary
activities (services), consumption of meat, divorce rates, tourism and holidays.
One can also state that, to some extent, the use of massive fossil energies
such as coal played an important role in the ending of slavery. One observes also
an evolution of transport means over time, with the availability of oil making the
transportation technologies more and more demanding (from bicycle to trains, buses,
personal cars and finally planes). Let us notice that there is a decreasing exponential
relationship between density of inhabitants in cities and the consumption of transports
per inhabitant. In other words, the denser the city, the more people walk.

GDP measures the flow of work produced by people and the flow of capital,
but does not measure other flows in the production, for example the negative impact
of pollution as well as depletion of resources (renewable and non-renewable).
Economic theories who were created two hundred years ago had no incentive to
consider physical constraints because human populations at this time had
a negligible impact on the Earth climate and pollution levels.

<br>

# COURSE 2 - Oil apocalypse or carbon paradise?

**Video link (in French):** [video course 2](https://www.youtube.com/watch?v=1NHPgrH5lcQ)

Contrary to what is often said, France is not a country which is fully dependent
on nuclear energy. In fact, industrial machines, tertiary and residential activities
take about half of their energy from fossil energy sources (especially from natural gas),
while transport and agriculture are almost fully dependent on oil.

Crude Oil is used to produce numerous products after refinery. Starting from a barrel
of crude oil, one usually produce:
- 3% of asphalt / bitumen: road construction.
- 4% of liquefied petroleum gas (LPG): a mixture of propane and butane used as heating fuels, transport fuels and feedstock for petrochemical industry.
- 10% of jet fuel.
- 23% of diesel fuel and heating oil: used in trucks, trains, boats, cars, heavy construction and electricity generators.
- 47% of gasoline and petrol: used in transportation, mostly cars.
- 18% of other products, i.e. naphta, waxes and lubricants: used as feedstock for the petrochemical industry.

These percentages depend on the structure of the refineries.

Oil is mainly used for transportation, and especially road transportation.
Also, 2/3 of oil extracted crosses a border to another country before being used
(for gas it is only 1/3). Hence, oil really is the energy of globalization.

Oil consumption is very unequal in different parts of the world.
The quantity of oil used by different countries is as follows:

| ![Oil consumption by countries](https://fabien-nugier.gitlab.io/blog/post/blog3/oil_countries_consumption.jpg) |
|:--:|
| <b>Oil consumption by countries. Source: jancovici.com.</b>|

The price of gas is indexed on the price of oil. This is because infrastructures to
transport gas are much less flexible than for oil, hence it would be costly for gas
infrastructures to see consumers change move their consumption towards oil when this
latter would drop in price.

Oil comes from the sedimentation of marine organisms like plants, algae, plankton
and bacterias, for which a small fraction falls to the bottom of oceans.
This happens more often near the coasts since they are more present at these locations
and they are less likely to get decomposed before reaching the bottom.
They then get mixed with shells and other dusts particles, and are sent deeper through
tectonic plates dynamics. It thus goes through hotter temperatures with time and
undergoes bacterial degradation. Depending on temperature, this creates coal,
methane, oil shales, etc. This process takes between 10 millions to 1 billion years.

| ![Gas consumption by countries](https://fabien-nugier.gitlab.io/blog/post/blog3/gas_countries_consumption.jpg) |
|:--:|
| <b>Gas consumption by countries. Source: jancovici.com.</b>|

Since oil and gas appear in geological formations able to contain them,
they often come with water and can be mixed with porous rocks such as limestone
or sand. At these sites, oil is present between few percents and few tens of percent,
typically around 10% (in mass). All the oil can't be extracted.
The more an oil reservoir is deep, the more fraction of gas it will have.

Detection and extraction of oil and gas is a complex endeavor and this involves large
uncertainties on the reserves, especially due to uncertainty on volumes of rocks
containing oil, porosity of the rock, distribution of oil and its viscosity.
However, one can be confident that most of the reserves in oil and gas are known,
with a significant part already extracted. This is not the case for coal,
for which reserves are much larger compared to what has already been extracted.

Despite new discoveries of oilfields, the number of discoveries is slowing down with
the years, and especially the new fields are smaller and smaller in capacity.
Many countries have passed their peaks of oil production, some of them
decades ago. However, new forms of reserves such as shale oil and oil sands have
also been discovered, but their extraction is more involved and less efficient.
The global peak on oil is believed to be around 2020-2025 (if not already passed)
and for gas around 2030-2040.

The price of an oil well varies depending on location. For example, drills in hostile
conditions (such as jungle, mountains, rough seas, deep offshore) can cost between USD 10M
and USD 30M, with depth going from 2 km to 4 km. On the other hand, offshore drills in easy
conditions cost between USD 3M and USD 10M, with depths from 2 to 3 km. For onshore drills
in easy conditions, drills can cost around USD 1M at 2km depth and up to USD 8M when going
down to 5 km.

It is a well known fact that the price of oil is not simply correlated with the
production through a supply / demand relationship. In addition, and for many reasons,
it is difficult to be sure about the size of oil reserves in each country of the world.
Hence, it is very difficult, if not impossible, to predict the future price of crude oil.

_**Note**_: this is a fact well explained in these two YouTube videos in French:
- [Prix du pétrole : comment les marchés suppriment les tendances - Heu?reka ](https://www.youtube.com/watch?v=wvt0S9b4IoY)
- [Prix négatifs du pétrole - Heu?reka](https://www.youtube.com/watch?v=Qf-VuCNLA8I)

Even though the price of oil cannot be predicted, the production can be. Indeed, discoveries
of oilfields predict pretty well the production a few decades ahead. New evaluations of
oil reserves could have an impact of price as market prices are supposed to be efficient,
i.e. reflecting all (publicly) available information at pricing time.
What seems important to notice is that global oil consumption is directly related to
global GDP, as shown below:

| ![Global Oil and GDP](https://fabien-nugier.gitlab.io/blog/post/blog3/global_oil_gdp.jpg) |
|:--:|
| <b>Global Oil and GDP. Source: jancovici.com.</b>|

In particular, the correlation is more apparent since the oil crises due to increased
globalization (mostly allowed through the use of oil in transportation) and energetic
efficiency which implies a stronger correlation between US dollar and a barrel of
crude oil (because efficiency of machines implies that less oil directly impose to
switch them off, while inefficient industry could still allow them to work).

As for coal, it is present on all continents, but very unevenly scattered in countries.
Hard coal is an ancient coal which is found at profound depth and is highly concentrated
in carbon while brown coal (or lignite) is younger, closer to the surface and less rich
in carbon (but rich in water and other non-combustible materials).
Coal reserves are distributed as follows:

| ![Coal reserves by country](https://fabien-nugier.gitlab.io/blog/post/blog3/coal_countries.jpg) |
|:--:|
| <b>Coal reserves by country. Source: jancovici.com, based on BP Statistical Review, 2018.</b>|

Since it is the energy resource which emits the most CO2 for a kWh of energy produced,
and that it is one of the most abundant energy resource in the world, we can state
that the political choices of countries appearing in this chart will play a very
important role in the fight against climate change. Furthermore, and contrary to oil,
coal is mostly a source of energy consumed domestically, i.e. rarely exported.

Combining oil, gas and coal, the world reserves of countries are the following:

| ![Fossil reserves by country](https://fabien-nugier.gitlab.io/blog/post/blog3/fossil_countries.jpg) |
|:--:|
| <b>Fossil reserves by country. Source: jancovici.com, based on BP Statistical Review, 2018.</b>|

<br>

_To be continued..._
