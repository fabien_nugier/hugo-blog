---
title: World graph
subtitle: Building a graph from the world map
date: 1921-09-26
tags: ["data science", "maps", "geospatial", "python"]
---

[**Date:** 2021-09-26]

![World Map](https://upload.wikimedia.org/wikipedia/commons/4/41/Simple_world_map.svg)

<!-- ---------- ---------- ---------- ---------- ---------- ---------- ---------- --->

In this first post, I would like to consider the world's map and ask how we could transform
this map into a graph. This question appears quite simple at first, but it is easy to
realise that some complications emerge.

A graph is made from vertices and edges linking them together. Having a graph of all
countries can be very useful when it comes to represent physical distances between these
countries, volumes of trades or other types of relationships. In the case of trade, we can
consider a directed graph since a country A will have exports to a country B which does not
match its imports.

However, one can realize difficulties emerging from this representation. First, the world
is a spheroid, and distances between countries are not unique quantities. Hence, rather
than speaking about one graph representing the world, we can have an infinity of graphs as
there are an infinity of maps we can draw distances on. Furthermore, there are different
types of distances that can be considered, so choices must be made and these choices
determine the charateristics of edges in the graph. Second, it may appear pretty simple
to locate Luxembourg or Singaore on a map, but where should we place the vertex of the
graph on a country like USA? Where should we place the dot? At the center of the US
territory? What about Alaska? Considering the barycenter of the two would significantly
shift the vertex towards the North-West. Same with France and French Guiana, where the
vertex would end up in the Atlantic Ocean. 

As we can see from these difficulties, building a graph from countries on a map is not
that trivial and requires some assumptions.


## Assumptions

In this work, we assume the following:

1. _Data represents all countries in the world._ In practice, some countries are missing.
This is obvious from the fact that we have 177 distinct names of countries in the dataset,
while the actual number of countries in the world is 195
(cf. [Wiki's list of countries](https://en.wikipedia.org/wiki/List_of_countries_and_dependencies_by_area)).

2. _There are no issues with the polygons._ This means we assume that the polygons are perfectly
representing the countries and their territories.




## Coding & Developments

### Study of the data set

The geospatial packages used here are the following:
```Python
from pyproj import CRS
import geopandas as gpd
from shapely.geometry import Point, Polygon, MultiPolygon
```
Data concerning world's countries is loaded in a simple manner, using GeoPandas:
```Python
path = gpd.datasets.get_path('naturalearth_lowres')
gdf = gpd.read_file(path)
```

Several coordinate reference systems (CRS) can be employed to display this data.
For example, using the Eckert IV projection:
```Python
_Eck4 = 'ESRI:54012'
_Eck4_crs = CRS.from_string(_Eck4)
gdf = gdf.to_crs(crs=_Eck4_crs)
gdf.plot();
```
we get the following map:  
![Eckert IV World Map](https://fabien-nugier.gitlab.io/blog/post/blog1/worldmap_eck4.jpg)  
We can check that the CRS is Eckert IV through the simple display in jupyter notebook:
```Python
gdf.crs
```
which returns the following information:
```
<Projected CRS: ESRI:54012>
Name: World_Eckert_IV
Axis Info [cartesian]:
- E[east]: Easting (metre)
- N[north]: Northing (metre)
Area of Use:
- name: World.
- bounds: (-180.0, -90.0, 180.0, 90.0)
Coordinate Operation:
- name: World_Eckert_IV
- method: Eckert IV
Datum: World Geodetic System 1984
- Ellipsoid: WGS 84
- Prime Meridian: Greenwich
```
After some simple cleaning of the dataset, we can extract the area of each country.
This is done through the simple command:
```Python
gdf['area (km^2)'] = gdf['geometry'].area / 1E6
```
and we obtain a distribution of the areas of countries from matplotlib:
```Python
import matplotlib.pyplot as plt

plt.figure(figsize=(8, 5))
gdf['area (km^2)'].hist(bins=100);
plt.title("Histogram of the area of countries.")
plt.xlabel("area [km^2]")
plt.ylabel("Counts")
plt.show()
```  
which shows an exponential distribution:
![Histogram Countries Area](https://fabien-nugier.gitlab.io/blog/post/blog1/hist_countries_area1.jpg)    
Taking the log of this quantity, we obtain the more readable histograms below:
![Histogram Countries Area in Log](https://fabien-nugier.gitlab.io/blog/post/blog1/hist_countries_area2.jpg)      
and this indicates a possible log-normal distribution.

Very interestingly, one can draw similar observation regarding the populations and growth
domestic product (GDP) of these countries. Indeed, both appear to display a fat tail such
as described by exponential distributions (note that these two quantities and the area
cannot take negative values). It is also worth to note that we can observe a strong
correlation between the Log(Population) and Log(GDP), demonstrating that countries with
large populations are more likely to develop competitive economies. The correlations between
Log(Area) and Log(Population) as well as between Log(GDP) and Log(Area) are less pronounced.
These simple observations can point towards interesting macroeconomic comparisons and relate
to the existence of fat tails in nature and economy, as studied in the paper
[Laherrere and Sornette 1998](https://arxiv.org/abs/cond-mat/9801293).


A simple manipulation of this data lead to the following ranking of country's area:

<center>

| Country | Area (millions km<sup>2</sup>) |
| :---: | :---: |
| Russia | 16.958 |
| Antarctica | 12.245 |
| Canada | 10.002 |
| United States of America | 9.521 |
| China | 9.429 |
| Brazil | 8.56 |
| Australia | 7.719 |
| India | 3.157 |
| Argentina | 2.791 |
| Kazakhstan | 2.726 |

</center>

And this can be compared to [Wikipedia's list of largest territories](https://en.wikipedia.org/wiki/List_of_countries_and_dependencies_by_area)
and we can observe that computed areas are pretty close the Wiki page. Differences
could be attributed, at least in parts, to the precisions of territories' borders as
well as projection effects.


### Splitting territories

Since we want to create a graph from the geospatial description of territories belonging
to countries, and since these territories are stored in multi-polygons, one need to split
these multi-polygons into simple polygons that describe the different territories of a
country. This is done by creating a new GeoDataFrame in the following way:

```Python
gdf2 = gpd.GeoDataFrame(columns=['continent', 'name', 'iso_a3', 'geometry', 'area (km^2)'])

for index, row in list(gdf.iterrows()):
    
    # If geometry is MultiPolygon, split it into individual Polygons
    if isinstance(row['geometry'], MultiPolygon):
        
        # Get the geometries
        geoms = row['geometry'].geoms
        
        # Split the MultiPolygon
        for i, geom in enumerate(geoms):
            data_to_add = {'continent': row['continent'],
                           'name': row['name'],
                           'iso_a3': row['iso_a3'] + '_' + str(i),
                           'geometry': geom,
                           'area (km^2)': geom.area / 1E6}
            gdf2 = gdf2.append(data_to_add, ignore_index=True)

    # If geometry is Polygon, keep it this way
    else:
        data_to_add = {'continent': row['continent'],
                       'name': row['name'],
                       'iso_a3': row['iso_a3'],
                       'geometry': row['geometry'],
                       'area (km^2)': row['geometry'].area / 1E6}
        gdf2 = gdf2.append(data_to_add, ignore_index=True)
```

Let us note that some countries have only one territory (e.g. Tanzania or Iceland) while some others
have many (e.g. Canada holding the record with 30 territories).

We select the territories according to their fraction of the total area of the country
using the following function:
```Python
def get_biggest_territories(geodata, area_fraction=0.05):
    """
    Create a GeoDataFrame from another one, computing area of territories
    and suppressing territories which are too small.
    
    Args:
        geodata: GeoDataFrame with columns 'name', 'area (km^2)' (and more).
        area_fraction (float): fraction of territories under which territories are discarded.
        
    Returns:
        GeoDataFrame: new dataframe with selected territories and 'area fraction' column.
    """
    
    # Checks
    assert isinstance(geodata, gpd.GeoDataFrame)
    assert isinstance(area_fraction, float)
    assert 0 <= area_fraction <= 1
    
    # Initialization
    new_gdf = geodata.copy()
    
    # Get total area of countries
    total_area = gdf[['name', 'area (km^2)']]
    
    # Go through territories and compute their fraction of country's area
    for index, row in list(geodata.iterrows()):
        tot_area = total_area[total_area['name'] == row['name']]['area (km^2)'].values[0]
        new_gdf.loc[index, 'frac_area'] = row['area (km^2)'] / tot_area

    # Proceed to selection
    new_gdf = new_gdf[new_gdf['frac_area'] > area_fraction]

    return new_gdf
```
which allows a selection within countries having multiple territories.

We can obtain the centroids from the geo-dataframe that has been generated and obtain the
following map:
![Centroids of main territories](https://fabien-nugier.gitlab.io/blog/post/blog1/centroids_main_territories.jpg)
As it can be seen on this map, only 2 territories out of 30 remain in the composition of Canada
when using the 5% fraction of country's area threshold. We can also see that USA has 2
territories which are the mainland and Alaska. Territories related to Hawai have disappeared.

For comparison, one can plot the world map with centroids of countries accounting for all
their territories:
![Centroids of main territories](https://fabien-nugier.gitlab.io/blog/post/blog1/centroids_countries.jpg)
As expected, we notice the vertex of USA is shifted towards North-West, we also notice the vertex of Canada
shifted towards the North. Maybe even more significant is France, when the vertex is
now above Spain because of Guiana making a large contribution to the centroid.


### Distances between vertices

Now we have positions of vertices, we can compute the distances "between countries".
More precisely, we have neglected small territories (with respect to their total area)
and are computing the distance between centroids of their main territories.

Since the maps displayed above and the data are using the popular CRS named
"World Geodetic System 1984" (WGS84), we need to convert data into a CRS able to
describe distances. We re-project the data into the "Eckert IV" CRS, but this is
an imperfect choice since Eckert IV does not conserve distances. Better projections
could be studied in the future.

Computing the distances between all territories lead to a matrix of distances. Having
207 territories, we end up having 207 x 207 distances to compute. To display the results
in a more readable fashion, let us focus on the 10 biggest territories in the world.
These territories are represented in the following map:
![Centroids of 10 biggest territories](https://fabien-nugier.gitlab.io/blog/post/blog1/centroids_10biggest.jpg)
The 10 x 10 matrix of distances between all the territories is the following:
![Matrix of distances between centroids](https://fabien-nugier.gitlab.io/blog/post/blog1/matrix_distances.jpg)
Now using Scipy's function to build dendrogram, we can group these territories and see if the
grouping makes sense. The result we obtain is as follows:
![Dendrogram of the matrix of distances](https://fabien-nugier.gitlab.io/blog/post/blog1/dendrogram.jpg)
One can recognize from the dendrogram that countries are in different continents.

Let us make several remark concerning these results. First, one should be aware that
our matrix of distances directly depend on the CRS used for projection. Second, one
should understand that our distances directly depend on the map which is drawn. Indeed,
as we mentioned already there are many distances that can be computed between the centroids.
For example, one can think computing the distance from the USA to China going West, crossing
the Pacific Ocean. Hence, distances are tied to the map which is considered.

### Building the graph

Using the package Networkx, we can create a graph from our set of the 10 biggest
territories. To avoid long displays of code, we only show the results here.

The graph that we generate has 10 nodes, each corresponding to a territory of the above
last map. We use the function `.touches()` of Shapely's Polygons to determine if two
territories are adjacent. If they are, then we will draw a link between them in the graph.
The width of these links (or "edges") is proportional to the inverse distance between
these adjacent territories. As for the size of the nodes, it is chosen to be
proportional to the area of the territory. Here is the graph that we obtain:
![Graph of 10 biggest territories](https://fabien-nugier.gitlab.io/blog/post/blog1/graph_10biggest.jpg)  
As we can see, Russia's centroid is closer to Kazakhstan's than it is to China's.
We can also notice that the size of nodes makes sense.

_Hence we obtained a graph from territories of the world map!_

## Conclusions

In this first blog, we've used simple geospatial data to split countries in multiple
territories, keep the main territories and compute their centroids. This allowed us to
compute distances between the centroids, with a clear dependence on the projection and
the map being considered. We have used this information to build a graph and show that
we can start with a map and end up with a graph, allowing descriptions of interaction
between territories. This kind of graph can turn out very useful when it comes to
describe international trade, transportation and migrations, among many other topics.
We will revisit this graph description approach in futures posts and allow more
systematic treatments.






