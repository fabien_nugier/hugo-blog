---
title: Countries' boundaries
subtitle: Extracting information about countries' borders.
date: 1921-10-03
tags: ["data science", "maps", "geospatial", "python"]
---

[**Date:** 2021-10-03]  


| ![World Map](https://fabien-nugier.gitlab.io/blog/post/blog2/world-map.gif) |
|:--:|
| <b>Source: geology.com</b>|

<!-- ---------- ---------- ---------- ---------- ---------- ---------- ---------- --->

When dealing with maps in Geopandas, countries are described by a set of Shapely's
polygons in a certain coordinate reference system (CRS). Since every map is drawn
with a certain scale and territories on it are drawn with a certain level of precision,
one question we can ask when we have a map is how, starting 
from a granular contour of a country, can we draw this country in a simpler manner?
In a more general fashion, one can wonder how to reduce the granularity of a map
representing several countries or territories. However, one needs to be careful that
territories which have common borders get the same border during the simplification.
Also, in some context, one does not want a simplification of borders that may
significantly affect the area of territories. All these questions point to a proper
analysis of territories' borders.

Aside from the technical aspect of territories' borders, one can also be interested
to know how many neighbors territories have, how much of their borders is shared with
neighboring countries rather than oceans or seas. One can even ask questions about the
shape of territories, like _which territory is the longest?_ or _which territory is closer
to a circle?_ Hence, analysis of territories and their borders can lead to significant economical,
environmental and geopolitical insights. 


## Assumptions

In this work, we stick to the World Geodetic System 1984 (WGS 84) for coordinates, which
is the CRS used by GPS. This means that distances will be computed in degrees and
surfaces in squared degrees. Since this CRS describes pretty well the Earth's geoid,
these quantities should be well represented, with only minor distortion effects.

We will assume that all countries are represented in the dataset (which is not the case)
and that all their territories are present in the dataset (which also isn't true). Since
only small countries or territories are likely to be missing, this does not represent a
large problem in terms of mapping at the scales considered here.


## Coding & Developments

We will address two topics in this article. The first one addresses territories' borders
and the question of simplifying them. The second one is a short study of territories'
neighbors and their shape.

### Simplifying a border

The data is loaded into a GeoPandas dataframe in a simple manner:
```Python
import geopandas as gpd

path = gpd.datasets.get_path('naturalearth_lowres')
gdf = gpd.read_file(path)
gdf['name'] = ['_'.join(x.split(' ')) for x in gdf['name'].values]
```
and we create a new geodataframe with all territories of countries separated into
different rows through the command:
```Python
gdf_exp = gdf.explode()
```
The advantage of this operation is that we can now deal with territories separately.
For example, when dealing with France, one gets 3 territories from the dataset (while in
reality much more exist), corresponding to the mainland, Corsica and French Guiana.

As an illustration, one can see that the first dataset treats all territories together:
```Python
import matplotlib.pyplot as plt

fr_geom = gdf[gdf['name'] == 'France']['geometry']

fig, ax = plt.subplots(figsize=(10, 10))
fr_geom.plot(ax=ax, alpha=1, color='green')
fr_geom.convex_hull.plot(ax=ax, alpha=0.2, color='blue')
plt.show()
```
leading to the following map:

| ![French territories](https://fabien-nugier.gitlab.io/blog/post/blog2/french_territories.jpg) |
|:--:|
| <b>French territories together</b>|

while the exploded dataset leads to a table with different rows representing each territory
separately:
```Python
fr_geom_exp = gdf_exp[gdf_exp['name'] == 'France']
fr_geom_exp
```
thus allowing to plot each of them separately (not presented here).

| ![Exploded data for France](https://fabien-nugier.gitlab.io/blog/post/blog2/french_data.jpg) |
|:--:|
| <b>Exploded geometry restricted to French territories</b>|

The mainland of France is then easily selected by:
```Python
fr_mainland = fr_geom_exp.loc[(43, 1)]
```
and we can proceed similarly for every territory in the dataset.

This allows us to manipulate the borders of territories. One function that we can think
of is a function in which we jump over some points of the territory's boundary, but
keeping the points which belong to the convex hull of the territory, thus keeping
some control on the overall shape of the territory itself.
One possible implementation of this function is as follows:
```Python
def get_simpler_boundary(geometry, n_jump):
    """
    Returns a geometry with less points than its initial geometry.
    The value of n_jump determines the number of points we jump over in the
    initial boundary in between points of the convex hull boundary.
    """
    
    # Checks
    assert isinstance(n_jump, int)
    assert n_jump > 0
    
    # Get the initial geometry coordinates
    x1, y1 = geometry.exterior.xy
    # Get the convex hull coordinates
    x2, y2 = geometry.convex_hull.exterior.xy

    # Pack them
    xy1 = list(zip(x1, y1))
    xy2 = list(zip(x2, y2))

    # Get their difference, i.e. the points
    # which are in the initial boundary
    # but not in the convex hull
    xy2diff1 = [xy for xy in xy1 if xy not in xy2]
    
    # Make a sub-sampling among these points
    subsampling = xy2diff1[0:-1:n_jump]
    
    # Make a new list of points for the new geometry
    xy_new = xy1
    for xy in xy_new:
        if (xy not in xy2) and (xy not in subsampling):
            xy_new.remove(xy)
    
    # Make the new geometry
    new_geometry = Polygon([*xy_new])
    
    return new_geometry
```
and can be used as follows to simplify France mainland's border:
```Python
# Compute the new boundary
france = get_simpler_boundary(geometry=fr_mainland.geometry, n_jump=10)
france_x, france_y = france.exterior.xy
check_convhull_x, check_convhull_y = france.convex_hull.exterior.xy

# Plot it
fig, ax = plt.subplots(figsize=(5, 5))
ax = plt.plot(x1, y1, alpha=0.5, color='green', lw=2)
ax = plt.plot(x2, y2, alpha=0.2, color='blue')
ax = plt.plot(france_x, france_y, color='red', linestyle='--')
plt.show()
```
which gives the following new border:

| ![Simplified border of France's mainland](https://fabien-nugier.gitlab.io/blog/post/blog2/france_mainland_simplify.jpg) |
|:--:|
| <b>Simplified border of France's mainland. Green line is France initial border, blue line
is its convex hull, while the dashed red line is the simplified border.</b>|

Obviously, such a function could be improved and more involved methods could be designed.
For example, one can think of simplifying the border by removing a single point, but
selecting this point in such a way that the total area of the territory is minimally
affected. Different optimization schemes can be developed.


### Analysing all countries' borders

In this section, we are not interested anymore by the simplification of territories'
borders, but instead consider the initial exploded data for all countries in order
to extract information about their neighborhoods.

We build a new dataset called _data_ that contains only the polygon of territories and
the name of their country, proceeding in the following way:
```Python
data = gdf_exp[['name', 'geometry']]
digits = [x[1] for x in data.index.values.tolist()]
data['index'] = [data['name'].values[y] + '_' + str(digits[y]) for y in range(len(digits))]
data.set_index('index', inplace=True)
```

Building on top of this new dataset, we can easily extract the number of neighbors they
have, and for each of these neighbors extract the coordinates of the common border (which
is a MultiLineString object of Shapely):
```Python
borders = []
nbr_to_plot = 5
nbr_plot = 0

terr_nbr_neighbors = []

# Go through the territories
for name1 in data.index:

    nbr_neighbors = 0
    for name2 in data.index:
        
        # Get the polygons
        poly1 = data.loc[name1, 'geometry']
        poly2 = data.loc[name2, 'geometry']
        
        # Check if they are neighbors
        is_neighbor = poly1.touches(poly2)

        # If they are, store their border
        if is_neighbor:
            common_border = poly1.intersection(poly2)
            border_length = common_border.length
            border = (name1, name2, poly1, poly2, common_border, border_length) # in degrees
            borders.append(border)
            nbr_neighbors += 1

        # Plot it (only limited number of cases)
        if is_neighbor and nbr_plot < nbr_to_plot:
            plot_border(poly1, poly2)
            nbr_plot += 1

    # Store the number of neighbors
    terr_nbr_neighbors.append((name1, nbr_neighbors))

# Make a dictionary
territories_nbr_neighbors = dict(terr_nbr_neighbors)
```

This piece of code uses the following function to plot two neighboring territories:
```Python
def plot_border(poly1, poly2):
    """
    Plot the border of two territories.
    """

    fig, ax = plt.subplots(figsize=(5, 5))

    # Get the border and its length
    border = poly1.intersection(poly2)
    border_length = border.length
    
    # First territory
    x1, y1 = poly1.exterior.xy
    ax = plt.plot(x1, y1, color='green')

    # Second territory
    x2, y2 = poly2.exterior.xy
    ax = plt.plot(x2, y2, color='blue')

    # Frontier (multilinestring)
    for n in range(len(border)):
        segment = border[n].xy
        ax = plt.plot(segment[0], segment[1], color='red')

    plt.title(f"Border of {name1} and {name2} has length = {round(border_length, 3)} degrees")
    plt.show()
```

For example, we can see that Tanzania and the Democratic Republic of Congo share a
common border of 4.258 degree in length (as measured from the Earth's center). Taking the
Earth radius as 6371 km, we get a length of $$6371 \times 4.258 \times \pi / 180 = 473 ~\mbox{km} ~.$$
A simple Google query returns 480 km, pretty close to the computed value.

| ![Tanzania and the Democratic Republic of Congo](https://fabien-nugier.gitlab.io/blog/post/blog2/Tanzania_DRC_neighbors.jpg) |
|:--:|
| <b>Tanzania and the Democratic Republic of Congo</b>|

Proceeding from above, all the borders of adjacent countries are computed consecutively
and stored.

One can build a dataframe with the cumulated length of borders (i.e. having a neighbor
country) for each concerned territory. This is done as follows:
```Python
cols_names = ['name1', 'name2', 'poly1', 'poly2', 'common_border', 'border_length']
df_borders = pd.DataFrame(index=None, data=borders, columns=cols_names)
df_borders['area1'] = [x.area for x in df_borders['poly1'].values]
borders_lengthes = df_borders[['name1', 'name2', 'border_length']].groupby(by='name1').sum()
borders_lengthes.sort_values(by='border_length', ascending=False, inplace=True)
borders_lengthes.index.name = 'name'
borders_lengthes.columns = ['border length (degrees)']
```

The first 10 entries of this dataframe give the territories with the longest borders:

| name                       |   border length (degrees) |
|:---------------------------|--------------------------:|
| Russia_1                   |                  179.448  |
| China_1                    |                  166.039  |
| Kazakhstan_0               |                  109.078  |
| Brazil_0                   |                   97.0079 |
| United_States_of_America_0 |                   91.0919 |
| Canada_0                   |                   90.8829 |
| India_0                    |                   81.1755 |
| Mongolia_0                 |                   78.7043 |
| Dem._Rep._Congo_0          |                   74.9373 |
| Argentina_1                |                   68.5636 |

As we can see from the map at the top of this post, the ranking of these territories
makes sense (where obviously the indices 0 and 1 associated to the territories here
don't matter as they correspond to the mainland of each country).

Digging into the data, we can see that territories which had no neighboring countries
disappeared. This is the case of Corsica for one of the territories of France, but
this is also the case of Australia and Japan which have no neighbor country touching them.

Proceeding in a similar manner (code not shown), we can extract more information about
these territories. For example, we can compute for each territory the ratio of their
border length (with other countries) and their total perimeter. One finds a value of
1 when a territory has no access to the sea, and this is the case for the following
territories:

> Armenia, Belarus_0, Bhutan, Bolivia, Burundi, Czechia, Hungary, Kyrgyzstan,
> Lesotho, Luxembourg, Moldova, Nepal, Palestine, Serbia, Slovakia, Tajikistan,
> Zimbabwe, eSwatini
> 
> Note: having checked that these countries had only one territory, we have removed the
> subscript "_0" that was associated to their name.

Among the territories having just small access to the sea, one can find the following
which neighbors take more than 95% of their territory's surrounding:

<sub>

| name              |   border_length (deg) |   territory_area (sqrd deg) |   territory_length (deg) |   ratio_neighbor_over_total |
|:------------------|----------------------:|----------------------------:|-------------------------:|----------------------------:|
| Dem._Rep._Congo_0 |              74.9373  |                   189.515   |                  75.2776 |                    0.995479 |
| Jordan_0          |              15.3862  |                     8.43917 |                  15.5348 |                    0.990436 |
| W._Sahara_0       |              27.2374  |                     8.60398 |                  27.6621 |                    0.984645 |
| Iraq_0            |              29.6859  |                    42.2294  |                  30.2814 |                    0.980335 |
| Slovenia_0        |               7.67686 |                     2.22531 |                   7.9172 |                    0.969643 |
| Congo_0           |              31.0167  |                    27.6207  |                  32.3569 |                    0.95858  |

</sub>

Following down in the ranking, one can see that France's mainland has a ratio of 47.1%
while the USA has a ratio of 49.6%. The mainland of France and the USA are thus split
almost perfectly in half between neighbors and the oceans or seas.

Having computed the number of neighbors of each territory earlier, and also having access
to their area, we can now study potential correlations between them and the "ratio of surrounding"
described above.

First, we observe that there is a correlation of 0.45 between the ratio of surrounding neighbors
and the number of neighbors. Two territories appear as outliers, with China's mainland
having 14 neighboring countries and Russia having 13, far above the next two territories
of the Democratic Republic of Congo and Brazil (both having 10 neighbors).

| ![Ratio of surrounding v.s. number of neighbors](https://fabien-nugier.gitlab.io/blog/post/blog2/surrounding_ratio_vs_nbr_neighbors.jpg) |
|:--:|
| <b>Ratio of surrounding v.s. number of neighbors</b>|

The correlation between the surrounding ratio and territories' area is much
less pronounced (-0.13):

| ![Ratio of surrounding v.s. territory's area](https://fabien-nugier.gitlab.io/blog/post/blog2/surrounding_ratio_vs_area.jpg) |
|:--:|
| <b>Ratio of surrounding v.s. territory's area</b>|

We can also notice that 5 territories have unnaturally large areas compared to the
bulk of countries:

<sub>

| name                       |   territory_area (sqrd deg) |   ratio_neighbor_over_total |   nbr_neighbors |
|:---------------------------|----------------------------:|----------------------------:|----------------:|
| Russia_1                   |                    2836.07  |                    0.319991 |              13 |
| Canada_0                   |                    1281.34  |                    0.287207 |               2 |
| China_1                    |                     951.651 |                    0.771589 |              14 |
| United_States_of_America_0 |                     839.764 |                    0.496083 |               2 |
| Brazil_0                   |                     710.185 |                    0.612247 |              10 |

</sub>

As we can see from the above table which has been ranked according to the area of territories,
Canada and USA have a very small number of neighbors compared to Russia, China and Brazil. 

Finally, we can notice a correlation of about 0.4 between the territories' area (or their log)
and the number of neighbors (both cases giving similar correlation coefficient). This
suggests the intuitive fact that the bigger a territory the more likely it will have
neighbors.


### Which territory looks the most like a circle?

To finish this post, let us explore a fun question. Indeed, one can wonder among all
the territories that we are considering (i.e. territories having neighbors), which of
these countries has a shape closer to a circle.

To estimate this, let us note the following fact:

> __The Isoperimetric Problem__: the circle is the 2-dimensional shape that maximizes
> the ratio of area over perimeter (in Euclidean space at least).
> 
> In other words, any polygon that deviates from a circular
shape will have a ratio "area / perimeter" which is larger than the one of a circle.

Notes: to learn more about this problem, you can consult the following:
- [Link 1](https://www.maa.org/sites/default/files/pdf/upload_library/22/Ford/blasjo526.pdf)
- [Link 2](https://www.caam.rice.edu/~rat/cv/tapia_euler.pdf)

This being true, we can also understand that for a constant perimeter, the circle is also
the shape that maximizes the ratio A(C)/C^2 where A is the area and C the perimeter.

We find that the roundest territories (with neighbors) in the world are the following:

<sub>

| name           |     A/C<sup>2</sup> |
|:---------------|----------:|
| eSwatini_0     | 0.0722816 |
| Luxembourg_0   | 0.0678936 |
| Qatar_0        | 0.0664272 |
| Sierra_Leone_0 | 0.0638896 |
| Lesotho_0      | 0.0638085 |

</sub>

Checking on a map, one can see that the results seem pretty fair. Let us notice
that this ratio of A/C^2 is equal to 1 / 4 $\pi$ for a circle, i.e. 0.0795.
This means that ESwatini is quite close to be a circle.


## Conclusions

In this blog post, we have used geospatial analysis to draw simpler borders for countries.
We have presented a simple function to do that and we could improve on this work to find
better methods. Another step would be to simplify borders of several countries together,
thus building a simpler map of the world.

In a second part, we have analysed all the borders of territories in the world, isolating
all borders between countries, computing their number of neighbors and their fraction of
border touching other territories. This allowed us to rank countries and extract some
information that can be relevant in economy, trade, population migration and other geopolitical
analysis. We have found some correlation between the size of territories or their "ratio of
surrounding" and the number of neighbors they have.

Finally, to end this post on a lighter note, we have searched territories that could be
the roundest possible. We found that ESwatini, a small country of 1.16 million inhabitants
(in 2020) is the roundest country in the world when excluding islands.

